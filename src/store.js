import Vue from "vue";
import Vuex from "vuex";
import VueRouter from 'vue-router';
Vue.use(VueRouter)
Vue.use(Vuex);
const state = {
  isAuthentication: false,
  photo: {
    x: 0,
    y: 0,
    w: 360,
    h: 360
  },
  profilePic: {
    x: 400,
    y: 20,
    w: 60,
    h: 60
  },
  titleIg: {
    x: 400,
    y: 100,
    w: 160,
    h: 40,
    textAttr : {
      textAlign: 'left',
      textColor: '#212529',
      backgroundColor: '#FFFFFF',
      fontFamily: 'Roboto',
      fontWeight: 400,
    },
  },
  textIg: {
    x: 400,
    y: 160,
    w: 220,
    h: 60,
    textAttr : {
      textAlign: 'left',
      textColor: '#212529',
      backgroundColor: '#FFFFFF',
      fontFamily: 'Roboto',
      fontWeight: 400,
    },
  },
  tagsIg: {
    x: 400,
    y: 240,
    w: 220,
    h: 60,
    textAttr : {
      textAlign: 'left',
      textColor: '#212529',
      backgroundColor: '#FFFFFF',
      fontFamily: 'Roboto',
      fontWeight: 400,
    },
  },
  elementActive: {
    photo : false,
    profilePic: false,
    titleIg: false,
    textIg: false,
    tagsIg: false
  },
  bgEditor: {
    color: '#FFFFFF',
  },
  instagramInfo: {},
  instagramData: {},
  
};

const mutations = {
  SET_ACTIVE_ELEMENT(state, payload) {
    for (var item in state.elementActive) {
      if(item == payload){
        state.elementActive[item] = true;
      } else {
        state.elementActive[item] = false;
      }
    }
  },
  SET_ORIGIN_X(state, payload) {
    for (var item in state.elementActive) {
      if(state.elementActive[item]){
        state[item].x = payload;
      } 
    }
  },
  SET_ORIGIN_Y(state, payload) {
    for (var item in state.elementActive) {
      if(state.elementActive[item]){
        state[item].y = payload;
      } 
    }
  },
  SET_DIMENTIONS_HEIGHT(state, payload) {
    for (var item in state.elementActive) {
      if(state.elementActive[item]){
        state[item].h = payload;
      } 
    }
  },
  SET_DIMENTIONS_WIDTH(state, payload) {
    for (var item in state.elementActive) {
      if(state.elementActive[item]){
        state[item].w = payload;
      } 
    }
  },
  UPDATE_ATTR_EL(state, payload) {
    for (var item in state.elementActive) {
      if(state.elementActive[item]){
        state[item].x = payload.x;
        state[item].y = payload.y;
        state[item].w = payload.w;
        state[item].h = payload.h;
      } 
    }
  },
  UPDATE_BG_COLOR(state, payload) {
    state.bgEditor.color = payload
  },
  LOAD_INSTAGRAM(state, payload) {
    state.instagramData = payload;
    state.isAuthentication = true;
  },
  SET_TEXT_ALIGN(state, payload){
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        state[item].textAttr.textAlign = payload;
      } 
    }
  },
  SET_FONT_FAMILY(state, payload){
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        state[item].textAttr.fontFamily = payload;
      } 
    }
  },
  SET_FONT_STYLE(state, payload){
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        state[item].textAttr.fontWeight = payload;
      } 
    }
  },
  SET_TEXT_BG_COLOR(state, payload){
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        state[item].textAttr.backgroundColor = payload;
      } 
    }
  },
  SET_TEXT_COLOR(state, payload){
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        state[item].textAttr.textColor = payload;
      } 
    }
  },
  SET_IG_TOKEN(state, payload) {
    state.instagramInfo = payload;
    state.isAuthentication = true;
  }
 
};

const actions = {
  setOriginX(context, number) {
    context.commit("SET_ORIGIN_X", number);
  },
  setOriginY(context, number) { 
    context.commit("SET_ORIGIN_Y", number);
  },
  setActiveEl(context, active) { 
    context.commit("SET_ACTIVE_ELEMENT", active);
  },
  setDimensionsHeight(context, number) {
    context.commit("SET_DIMENTIONS_HEIGHT", number);
  },
  setDimensionsWidth(context, number) {
    context.commit("SET_DIMENTIONS_WIDTH", number);
  },
  updateAttrEl(context, payload){
    context.commit("UPDATE_ATTR_EL", payload);
  },
  updateBgColorEditor(context, payload){
    context.commit("UPDATE_BG_COLOR", payload);
  },
  getInstagramToken(context, payload){
    var instagramURL = 'https://api.instagram.com/oauth/access_token';
    let formData = new FormData();
    formData.append('client_id','da72f68bea1140499b615b1eb683ad6e')
    formData.append('client_secret','429166074aad4fe9be28ae4dde032f85')
    formData.append('redirect_uri','http://localhost:8080/')
    formData.append('grant_type','authorization_code')
    formData.append('code',payload)
    fetch(instagramURL, {
      method: 'post',
      body: formData
      }).then(res=>{
        return res.json();
      }).then(jsonData => {
        if(jsonData && jsonData.access_token) {
          context.commit("SET_IG_TOKEN", jsonData);
          this.dispatch('loadInstagram',jsonData)
          //localStorage.setItem('instagramInfo',JSON.stringify(jsonData));
        }
    })

  },
  loadInstagram(context, igInfo) {
    let instagramURL = `https://api.instagram.com/v1/users/${igInfo.user.id}/media/recent/?access_token=${igInfo.access_token}&count=1`;
      fetch(instagramURL, {
        method: 'get'
        }).then(res=>{
          return res.json();
        }).then(jsonData => {
          let instagramData = jsonData.data[0];
          context.commit('LOAD_INSTAGRAM', instagramData)
      })
  },
  setTextAlign(context, payload){
    context.commit("SET_TEXT_ALIGN", payload);
  },
  setFontFamily(context, payload){
    context.commit("SET_FONT_FAMILY", payload);
  },
  setFontStyle(context, payload){
    context.commit("SET_FONT_STYLE", payload);
  },
  setTextBgColor(context, payload){
    context.commit("SET_TEXT_BG_COLOR", payload);
  },
  setTextColor(context, payload) {
    context.commit("SET_TEXT_COLOR", payload);
  }
};

const getters = {
  getState(state){
    return state;
  },
  getPhoto(state) {
    return state.photo;
  },
  getProfilePic(state) {
    return state.profilePic;
  },
  getTitleIg(state) {
    return state.titleIg;
  },
  getTextIg(state) {
    return state.textIg;
  },
  getTagsIg(state) {
    return state.tagsIg;
  },
  getElementActive(state) {
    return state.elementActive;
  },
  getBgColorEditor(state) {
    return state.bgEditor;
  },
  getInstagramData(state) {
    return state.instagramData;
  },
  getBgTextColor(state) {
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        return state[item].textAttr.backgroundColor;
      } 
    }
  },
  getTextColor(state) {
    for (var item in state.elementActive) {
      if(state.elementActive[item] == true){
        return state[item].textAttr.textColor;
      } 
    }
  },
  getAuthentication(state) {
    return state.isAuthentication
  }
};

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
});
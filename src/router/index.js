import Vue from 'vue'
import Router from 'vue-router'
import InstagramEditor from '@/components/InstagramEditor'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'InstagramEditor',
      component: InstagramEditor
    }
    
  ]
})
import Vue from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import VueDragResize from 'vue-drag-resize'
// Vue.component('vue-drag-resize', VueDragResize)
import VueDraggableResizable from 'vue-draggable-resizable'
import 'vue-draggable-resizable/dist/VueDraggableResizable.css'
import store from "./store";
// import router from './router'
Vue.component('vue-draggable-resizable', VueDraggableResizable)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')

module.exports = {
  outputDir: '../dist',
  productionSourceMap: false,
  filenameHashing: false,
};
